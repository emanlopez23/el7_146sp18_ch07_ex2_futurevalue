/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author el7
 */
/*
7-2 step 5: started new class FinancialCalculations.
moved calculateFutureValue here from futureValueApp
*/
public class FinancialCalculations {
    
        public static double calculateFutureValue(double monthlyInvestment,
            double monthlyInterestRate, int months) {
        double futureValue = 0;
        for (int i = 1; i <= months; i++) {
            futureValue
                    = (futureValue + monthlyInvestment)
                    * (1 + monthlyInterestRate);
        }
        return futureValue;
    }//end method calculateFutureValue
    
}
